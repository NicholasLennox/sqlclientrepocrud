﻿using SQLClientCRUDRepo.Models;
using SQLClientCRUDRepo.Repositories;
using System;
using System.Collections.Generic;

namespace SQLClientCRUDRepo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // Simulating Dependency Injection (highly simplified)
            ICustomerRepository repository = new CustomerRepository();
            // Passing dependency
            TestSelectAll(repository);
            TestSelect(repository);
        }

        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestSelect(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer("ZZZZZ"));
        }

        static void TestInsert(ICustomerRepository repository)
        {

        }

        static void TestUpdate(ICustomerRepository repository)
        {

        }

        static void TestDelete(ICustomerRepository repository)
        {

        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- { customer.CustomerID} {customer.ContactName} {customer.CompanyName} {customer.City} ---");
        }
    }
}
