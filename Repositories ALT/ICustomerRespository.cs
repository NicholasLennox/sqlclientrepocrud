﻿using SQLClientCRUDRepo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientCRUDRepo.Repositories_ALT
{
    public interface ICustomerRespository : IRepository<Customer>
    {
        Customer GetHighestSpender();
    }
}
