﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientCRUDRepo.Repositories_ALT
{
    public interface IRepository<T>
    {
        T GetById();
        IEnumerable<T> GetAll();
        bool Add(T entity);
        bool Edit(T entity);
        bool Delete(T entity);
    }
}
